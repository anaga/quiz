def solution(A):
    reversing = 0
    n = len(A)
    temp = [k for k in A]
    if n > 2:
        for i in range(1, n - 1):
            if temp[i - 1] == temp[i] and temp[i - 1] != temp[i + 1]:
                temp[i - 1] = temp[i + 1]
                reversing += 1
            elif temp[i - 1] != temp[i] and temp[i - 1] != temp[i + 1]:
                temp[i + 1] = temp[i - 1]
                reversing += 1
            elif temp[i - 1] == temp[i] and temp[i - 1] == temp[i + 1]:
                if temp[i] == 0:
                    temp[i] = 1
                else:
                    temp[i] = 0
                reversing += 1
        return reversing
    elif n == 2:
        if temp[0] == temp[1]:
            return 1
        else:
            return 0
    else:
        return 0
